<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="templates/header.jsp"/>

<h1 align="center">Search results</h1>
<h4 align="center">Phrase: "<c:out value="${searchPhrase}"/>"</h4>
<br/>

<table class="table" align="center" style="width: 60%">
    <tr>
        <td>Page</td>
        <td>Date of index</td>
        <td>Different words</td>
        <td>All words</td>
        <td>Details</td>
    </tr>

    <c:forEach items="${searchResults}" var="row">
        <tr>
            <td><a href="<c:url value="${row[1]}"/>"><c:out value="${row[1]}"/></a></td>
            <td><c:out value="${row[2]}"/></td>
            <td><c:out value="${row[3]}"/></td>
            <td><c:out value="${row[4]}"/></td>
            <td><a href="${contextPath}/url/<c:out value="${row[0]}"/>">details</a></td>
        </tr>
    </c:forEach>

</table>
</center>