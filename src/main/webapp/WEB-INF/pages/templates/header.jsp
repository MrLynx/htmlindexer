<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
<head>
  <title>HTML Indexer</title>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.0/cosmo/bootstrap.min.css">
  <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<style>
  .glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -ms-animation: spin .7s infinite linear;
    -webkit-animation: spinw .7s infinite linear;
    -moz-animation: spinm .7s infinite linear;
  }

  @keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
  }
  @-webkit-keyframes spinw {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
  }

  @-moz-keyframes spinm {
    from { -moz-transform: rotate(0deg);}
    to { -moz-transform: rotate(360deg);}
  }
</style>

<nav class="navbar navbar-default" >
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${contextPath}/">HTML-Indexer</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="${contextPath}/">List indexed pages</a></li>
        <li><a href="${contextPath}/add">Add new page</a> </li>
        <li><a href="${contextPath}/addmany">Add many pages recursive</a> </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li>
        <form method="post" class="navbar-form navbar-left" role="search" action="${contextPath}/search">
          <div class="form-group">
            <input name="searchPhrase" type="text" class="form-control" placeholder="Search phrase">
          </div>
          <button type="submit" class="btn btn-default" aria-label="Right Align">
            Search&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
          </button>
        </form>
        </li>
      </ul>
    </div>
</nav>