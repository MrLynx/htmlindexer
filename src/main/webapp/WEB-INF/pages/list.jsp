<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="templates/header.jsp"/>

<style>
  #addlink {
    /*font-size: larger;*/
    position: absolute;
    margin-right: 50px;
    margin-bottom: 20px;
    bottom: 0;
    right: 0;
  }

  #leftdiv {
    position: absolute;
    margin-left: 50px;
    margin-bottom: 20px;
    bottom: 0;
    left: 0;
  }
</style>

  <h1 align="center">All indexed pages:</h1>
  <br/>

<c:if test="${added}">
  <center>
  <div class="alert alert-success alert-dismissible" role="alert" style="width: 60%">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <strong >Success!&nbsp&nbsp</strong>Successfully indexed page.
  </div>
  </center>
</c:if>

  <table class="table" align="center" style="width: 60%">
    <tr>
      <td>Page</td>
      <td>Date of index</td>
      <td>Sentences</td>
      <td>Details</td>
    </tr>

    <c:forEach items="${urls}" var="row">
      <tr>
        <td><a href="<c:url value="${row[1]}"/>"><c:out value="${row[1]}"/></a></td>
        <td><c:out value="${row[3]}"/></td>
        <td><c:out value="${row[2]}"/></td>
        <td><a href="${contextPath}/url/<c:out value="${row[0]}"/>">details</a></td>

      </tr>
    </c:forEach>

  </table>
  </center>

<div id="leftdiv">
  <form method="post" action="${contextPath}/">
    <input name="orderby" value="date" type="hidden"/>
    <button type="submit" class="btn btn-default" aria-label="Right Align" style="width: 180px;">
      Order by date&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span>
    </button>
  </form>

  <form method="post" action="${contextPath}/">
    <input name="orderby" value="sentences" type="hidden"/>
    <button type="submit" class="btn btn-default" aria-label="Right Align" style="width: 180px;">
      Order by sentences&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span>
    </button>
  </form>
</div>


<div id="addlink">
  <form method="get" action="${contextPath}/add" >
    <button type="submit" class="btn btn-default" aria-label="Right Align" style="width: 180px;">
      Add new page&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </button>
  </form>

  <form method="get" action="${contextPath}/addmany" >
    <button type="submit" class="btn btn-default" aria-label="Right Align" style="width: 180px;">
      Add many pages&nbsp&nbsp&nbsp
      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </button>
  </form>

</div>

</body>
</html>
