<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="templates/header.jsp"/>

<style>
    #loading {
        font-size: x-large;
        position: absolute;
        float: left;
        left: 20px;
        top: 70px;
    }
</style>

<h1 align="center" style="margin-bottom: 30px">Add new page:</h1>
<div align="center" >
    <form method="post" action="${contextPath}/add" >
        <center>
        <div class="input-group" style="width: 60%" align="center">
            <c:choose>
                <c:when test="${validError}">
                    <div style="width: 100%">
                        <span class="input-group-btn has-error has-feedback">
                            <input type="text" class="form-control" name="urlString" id="inputError2" placeholder="${error_msg}" style="display: inline-block; float: left; max-width: 80%">
                            <button id="loading-btn" class="btn btn-default" type="submit" style="width: 20%; display: inline-block; float: right">
                                Index page&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-tag"></span>
                            </button>
                        </span>
                    </div>
                </c:when>
                <c:otherwise>
                    <div align="center" style="width: 100%">
                        <span class="input-group-btn" >
                            <input class="form-control" type="text" name="urlString" style="display: inline-block; float: left; max-width: 80%"/>
                            <button id="loading-btn" class="btn btn-default" type="submit" style="width: 20%; display: inline-block; float: right">
                                Index page&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-tag"></span>
                            </button>
                        </span>
                    </div>


                </c:otherwise>
            </c:choose>
        </div>
        </center>
    </form>

    <div id="loading-div" style="display: none">
        <span id="loading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
    </div>

    <script>
        $("#loading-btn").click(function () {
            $("#loading-div").show();
        });
    </script>
</div>

</body>
</html>
