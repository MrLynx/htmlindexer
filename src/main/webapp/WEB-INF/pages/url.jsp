<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="templates/header.jsp"/>

  <h1 align="center">"<c:out value="${url.url}"/>" - sentences and words </h1>
  <ol>
  <c:forEach items="${sentences}" var="sentence">
      <li>
        <c:forEach items="${sentence.words}" var="word">
          "<c:out value="${word.content}"/>",&nbsp
        </c:forEach>
      </li>
  </c:forEach>
  </ol>

  <a href="${contextPath}/">Back to list of indexed pages</a>

</body>
</html>
