<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="templates/header.jsp"/>

<style>
  #loading {
    font-size: x-large;
    position: absolute;
    float: left;
    left: 20px;
    top: 70px;
  }
</style>

<div align="center" >
  <h1 align="center" style="margin-bottom: 30px">Add new pages recursive:</h1>
  <form method="post" action="${contextPath}/addmany">
    <center>
      <div class="input-group" style="width: 60%" align="center">

        <c:choose>
          <c:when test="${validError}">
            <div style="width: 100%">
                <span class="input-group-btn has-error has-feedback">
                  <input type="text" class="form-control" name="urlString" id="inputError3" placeholder="${error_msg}" style="display: inline-block; float: left; max-width: 80%">
                  <button id="loading-btn" class="btn btn-default" type="submit" style="width: 20%; display: inline-block; float: right">
                    Index pages&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-tags"></span>
                  </button>
                </span>
            </div>

          </c:when>

          <c:otherwise>
            <div align="center" style="width: 100%">
              <span class="input-group-btn" >
                <input class="form-control" type="text" name="urlString" style="display: inline-block; float: left; max-width: 80%"/>
                  <button id="loading-btn" class="btn btn-default" type="submit" style="width: 20%; display: inline-block; float: right">
                    Index pages&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-tags"></span>
                  </button>
              </span>
            </div>
          </c:otherwise>
        </c:choose>

        <div align="center" style="width: 100%">
          <label for="depth" style="width: 300px; display: inline-block; text-align: match-parent; font-size: large; margin-top: 20px; margin-bottom: 20px;">Depth</label>
          <input id="depth" type="number" name="depth" class="form-control bfh-number" style="width: 100%" value="5"/>
          <br/><br/>
          <label for="max" style="width: 300px; display: inline-block; text-align: match-parent; font-size: large; margin-top: 20px; margin-bottom: 20px;">Maximum number of indexed pages</label>
          <input id="max" type="number" name="max" class="form-control bfh-number" style="width: 100%" value="50"/>
        </div>

      </div>
    </center>
  </form>

  <div id="loading-div" style="display: none">
    <span id="loading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
  </div>

  <script>
    $("#loading-btn").click(function () {
      $("#loading-div").show();
    });
  </script>
</div>

</body>
</html>
