package agh.bd.indexer.service;

import agh.bd.indexer.model.Sentence;
import agh.bd.indexer.model.Url;
import agh.bd.indexer.model.Word;
import agh.bd.indexer.persistence.SentenceDAO;
import agh.bd.indexer.persistence.UrlDAO;
import agh.bd.indexer.persistence.WordDAO;
import org.hibernate.Hibernate;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UrlService {
    @Autowired private SentenceDAO sentenceDAO;
    @Autowired private UrlDAO urlDAO;
    @Autowired private WordDAO wordDAO;

    private static Pattern pattern = Pattern.compile("(?:http://|https://)?(?:www.)?(?<url>[^?/#:\r\n]+)(?:/[^\r\n]*)?");
    private Set<String> indexedPagesSet = new HashSet<String>();
    private int pagesToIndex = 0;

    private AtomicInteger maxPages;
    private AtomicInteger processedPages;

    @Transactional
    public void indexUrl(String urlString) throws Exception {
        urlString = getDomainName(urlString);

        if (urlString == null) {
            throw new IllegalArgumentException("Bad URL");
        }

        Url url = urlDAO.getByUrl(urlString);
        if (url == null) {
            url = new Url();
            url.setDate(new Date());
            url.setUrl(urlString);
            urlDAO.add(url);
        }
        else {
            throw new Exception("ALREADY INSERTED");
        }

        Set<Sentence> sentences = getSentencesForUrl(url);
        for (Sentence sentence : sentences) {
            sentenceDAO.add(sentence);
        }
    }

    @Transactional
    public int indexManyPages(String url, int depth, int pagesToIndex) throws Exception {
        Document document = Jsoup.connect(getDomainName(url)).get();
        this.pagesToIndex = pagesToIndex;
        this.maxPages = new AtomicInteger(pagesToIndex);
        this.processedPages = new AtomicInteger(0);
        return indexUrlRecursively(url, depth);
    }

    @Transactional
    public int indexUrlRecursively(String url, int depth) throws Exception {
        if (depth == 0 || url == null || pagesToIndex <= 0) {
            return 0;
        }
        int indexedPages = 0;
        String domainName = getDomainName(url);
        if (domainName == null) {
            throw new Exception("Bad URL");
        }

        try {
            Document document = Jsoup.connect(domainName).get();
            Collection<Element> links = document.select("a[href]");

            for (Element link : links) {
                if (pagesToIndex <= 0) break;

                String linkUrl = getDomainName(link);

                if (linkUrl != null && !indexedPagesSet.contains(linkUrl)) {
                    indexUrl(linkUrl);
                    indexedPagesSet.add(linkUrl);
                    indexedPages++;
                    pagesToIndex--;
                    processedPages.incrementAndGet();
                    System.out.println(indexedPagesSet);
                    System.out.println("indexed page: " + linkUrl + "\tdepth: " + depth + "set: " + indexedPagesSet.size() + "AD: " + pagesToIndex);
                }
            }

            for (Element link : links) {
                if (pagesToIndex <= 0) break;
                String linkUrl = getDomainName(link);
                indexedPages += indexUrlRecursively(linkUrl, depth - 1);
            }

        } catch (Exception e) {
            return 0;
        }

        return indexedPages;
    }


    private String getDomainName(Element link) {
        Matcher matcher = pattern.matcher(link.attr("abs:href"));
        if (matcher.matches()) {
            String url = matcher.group("url");
            return ("http://" + url).toLowerCase();
        }
        return null;
    }

    private String getDomainName(String link) {
        Matcher matcher = pattern.matcher(link);
        if (matcher.matches()) {
            String url = matcher.group("url");
            return ("http://" + url).toLowerCase();
        }
        return null;
    }

    public List<Object[]> getUrlsByDate() {
        return urlDAO.getAllByDate();
    }

    public List<Object[]> getUrlsBySentences() {
        return urlDAO.getAllBySentences();
    }

    @Transactional
    private HashSet<Sentence> getSentencesForUrl(Url url) throws Exception {
        HashSet<Sentence> sentences = new HashSet<Sentence>();

        Document document = Jsoup.connect(url.getUrl()).get();
        for (Element element : document.body().select("*")) {
            if (element.ownText().trim().length() > 1) {
                HashSet<String> elementContentSet = new HashSet<String>();
                elementContentSet.addAll(Arrays.asList(element.ownText().split("\\.")));
                for (String sentenceContent : elementContentSet) {

                    Sentence sentence = new Sentence();
                    sentence.setWords(new HashSet<Word>());
                    sentence.setUrl(url);

                    HashSet<String> wordContentSet = new HashSet<String>();
                    String[] wordContentList = sentenceContent.split("\\s+");
                    for (int i = 0; i < wordContentList.length; i++) {
                        wordContentList[i] = wordContentList[i].replaceAll("[^\\w]", "").toLowerCase();
                    }
                    wordContentSet.addAll(Arrays.asList(wordContentList));

                    for (String wordContent : wordContentSet) {
                        Word word = wordDAO.getByID(wordContent);
                        if (word == null) {
                            word = new Word();
                            word.setContent(wordContent);
                            wordDAO.add(word);
                        }
                        sentence.getWords().add(word);
                    }

                    sentences.add(sentence);
                }
            }
        }

        return sentences;
    }

    public List<Object[]> searchByWords(String searchPhrase) {
        return urlDAO.searchUrlBySentences(searchPhrase);
    }

    @Transactional(readOnly = true)
    public Url getDetailedUrlById(long id) {
        Url url = urlDAO.getByID(id);
        Hibernate.initialize(url.getSentences());

        for (Sentence sentence : url.getSentences()) {
            Hibernate.initialize(sentence.getWords());
        }
        return url;
    }

    public AtomicInteger getMaxPages() {
        return maxPages;
    }

    public AtomicInteger getProcessedPages() {
        return processedPages;
    }
}











