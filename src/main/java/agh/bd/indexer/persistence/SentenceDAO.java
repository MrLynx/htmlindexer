package agh.bd.indexer.persistence;

import agh.bd.indexer.model.Sentence;


public class SentenceDAO extends GenericDAO<Sentence> {

    public SentenceDAO() {
        entityClass = Sentence.class;
    }

}
