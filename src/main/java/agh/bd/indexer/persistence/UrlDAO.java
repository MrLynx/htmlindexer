package agh.bd.indexer.persistence;

import agh.bd.indexer.model.Url;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class UrlDAO extends GenericDAO<Url> {

    public UrlDAO() {
        entityClass = Url.class;
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public List<Object[]> searchUrlBySentences(String searchPhrase) {
        String[] words = searchPhrase.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[^\\w]", "");
        }
        List<String> wordList = Arrays.asList(words);

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u.id, u.url, u.date, count(distinct w.content) as words_in_index, count(w.id) as distinct_words_found from Url u join u.sentences s join s.words w where w.content in (:words) group by u.id order by count(distinct w.content) desc");
        query.setParameterList("words", wordList);
        List<Object[]> result = query.list();
        List<Object[]> rows = new LinkedList<Object[]>();
        for (Object[] r : result) {
            rows.add(new Object[]{r[0], r[1], r[2], r[3], r[4]});
        }
        return rows;
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public List<Object[]> getAllByDate() {
        Session session = sessionFactory.getCurrentSession();
        List<Object[]> result = session.createQuery("SELECT u.id, u.url, count(s.id), u.date AS sentences FROM Url u INNER JOIN u.sentences s GROUP BY u.id ORDER BY u.date DESC").list();
        List<Object[]> rows = new LinkedList<Object[]>();
        for (Object[] r : result) {
            rows.add(new Object[]{r[0], r[1], r[2], r[3]});
        }
        return rows;
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public List<Object[]> getAllBySentences() {
        Session session = sessionFactory.getCurrentSession();
        List<Object[]> result = session.createQuery("SELECT u.id, u.url, count(s.id), u.date AS sentences FROM Url u INNER JOIN u.sentences s GROUP BY u.id ORDER BY count(s.id) DESC").list();
        List<Object[]> rows = new LinkedList<Object[]>();
        for (Object[] r : result) {
            rows.add(new Object[]{r[0], r[1], r[2], r[3]});
        }
        return rows;
    }

    @Transactional
    public Url getByUrl(String url) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT u FROM Url u WHERE u.url = (:url)");
        query.setParameter("url", url);
        List<Url> result = query.list();
        if (result == null || result.size() == 0) {
            return null;
        }
        return (Url) result.get(0);
    }

}
