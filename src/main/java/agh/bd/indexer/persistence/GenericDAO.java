package agh.bd.indexer.persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Repository
public abstract class GenericDAO<T> {

    @Autowired
    protected SessionFactory sessionFactory;
    protected Class<T> entityClass;

    @Transactional
    @SuppressWarnings("unchecked")
    public T getByID(Serializable ID) {
        Session session = sessionFactory.getCurrentSession();
        T result = (T) session.get(entityClass, ID);
        return result;
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<T> result = session.createQuery("FROM " + entityClass.getName()).list();
        return result;
    }

    @Transactional
    public void delete(T target) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(target);
    }

    @Transactional
    public void add(T target) {
        Session session = sessionFactory.getCurrentSession();
        session.save(target);
    }

    @Transactional
    public void update(T target) {
        Session session = sessionFactory.getCurrentSession();
        session.update(target);
    }

}
