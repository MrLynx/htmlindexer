package agh.bd.indexer.persistence;

import agh.bd.indexer.model.Word;

public class WordDAO extends GenericDAO<Word> {
    public WordDAO() {
        entityClass = Word.class;
    }

}
