package agh.bd.indexer.model;

import java.util.Set;

public class Sentence {
    private long id;
    private Set<Word> words;
    private Url url;

    public Sentence() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Word> getWords() {
        return words;
    }

    public void setWords(Set<Word> words) {
        this.words = words;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }
}
