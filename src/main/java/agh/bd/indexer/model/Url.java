package agh.bd.indexer.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Url {
    private long id;
    private String url;
    private Date date;
    private Set<Sentence> sentences = new HashSet<Sentence>();

    public Url() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(Set<Sentence> sentences) {
        this.sentences = sentences;
    }
}
