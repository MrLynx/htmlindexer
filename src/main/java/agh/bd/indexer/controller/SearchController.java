package agh.bd.indexer.controller;

import agh.bd.indexer.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {
    @Autowired private UrlService urlService;

    @RequestMapping(method = RequestMethod.POST)
    public String search(@RequestParam(value = "searchPhrase") String searchPhrase, ModelMap model) {
        List<Object[]> searchResults = urlService.searchByWords(searchPhrase);
        model.addAttribute("searchResults", searchResults);
        model.addAttribute("searchPhrase", searchPhrase);
        return "searchResult";
    }
}
