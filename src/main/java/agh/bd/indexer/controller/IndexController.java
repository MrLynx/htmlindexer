package agh.bd.indexer.controller;

import agh.bd.indexer.persistence.UrlDAO;
import agh.bd.indexer.service.UrlService;
import org.jsoup.HttpStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.UnknownHostException;

@Controller
@RequestMapping("/")
public class IndexController {

    @Autowired private UrlService urlService;
    @Autowired private UrlDAO urlDAO;

    @RequestMapping(method = RequestMethod.GET)
    public String listIndexedPages(@RequestParam(value = "added", required = false) Boolean added, ModelMap model) {
        model.addAttribute("added", added);
        model.addAttribute("urls", urlService.getUrlsByDate());
        return "list";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String listOrderedIndexedPages(
            @RequestParam(value = "added", required = false) Boolean added,
            @RequestParam(value = "orderby", required = false) String orderby,
            ModelMap model) {

        if (orderby.equals("date")) {
            model.addAttribute("urls", urlService.getUrlsByDate());
        }
        else if (orderby.equals("sentences")) {
            model.addAttribute("urls", urlService.getUrlsBySentences());
        }

        model.addAttribute("added", added);
        return "list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String addPageForm(
            @RequestParam(value = "validError", required = false) Boolean validError,
            @RequestParam(value = "error_msg", required = false) String error_msg,
            ModelMap model) {

        model.addAttribute("validError", validError);
        model.addAttribute("error_msg", error_msg);
        model.addAttribute("addPage", "add");
        return "addForm";
    }

    @RequestMapping(value = "addmany", method = RequestMethod.GET)
    public String addManyPagesForm(
            @RequestParam(value = "validError", required = false) Boolean validError,
            @RequestParam(value = "error_msg", required = false) String error_msg,
            ModelMap model) {

        model.addAttribute("validError", validError);
        model.addAttribute("error_msg", error_msg);
        model.addAttribute("addPage", "add");
        return "addManyForm";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addPageToIndex(@RequestParam("urlString") String urlString, ModelMap model) {
        try {
            urlService.indexUrl(urlString);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            model.addAttribute("validError", true);
            model.addAttribute("error_msg", "Bad page url");
            return "redirect:/add";
        } catch (UnknownHostException e) {
            e.printStackTrace();
            model.addAttribute("validError", true);
            model.addAttribute("error_msg", "Page not found");
            return "redirect:/add";
        } catch(HttpStatusException e) {
            e.printStackTrace();
            model.addAttribute("validError", true);
            model.addAttribute("error_msg", "404 Not Found");
            return "redirect:/add";
        } catch (Exception e) {
            if(e.getMessage().equals("ALREADY INSERTED")) {
                model.addAttribute("error_msg", "Page already indexed");
            }
            else {
                model.addAttribute("error_msg", "Error indexing page");
            }
            e.printStackTrace();
            model.addAttribute("validError", true);
            return "redirect:/add";
        }
        model.addAttribute("added", true);
        return "redirect:/";
    }

    @RequestMapping(value = "addmany", method = RequestMethod.POST)
    public String addManyPagesToIndex(@RequestParam("urlString") String urlString,
                                      @RequestParam("depth") int depth,
                                      @RequestParam("max") int max,
                                      ModelMap model) {

        try {
            int pagesIndexed = urlService.indexManyPages(urlString, depth, max);
            System.out.println("PAGES INDEXED: " + pagesIndexed);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            model.addAttribute("validError", true);
            model.addAttribute("error_msg", "Bad page url");
            return "redirect:/addmany";
        } catch (UnknownHostException e) {
            e.printStackTrace();
            model.addAttribute("validError", true);
            model.addAttribute("error_msg", "Page not found");
            return "redirect:/addmany";
        } catch(HttpStatusException e) {
            e.printStackTrace();
            model.addAttribute("validError", true);
            model.addAttribute("error_msg", "404 Not Found");
            return "redirect:/addmany";
        } catch (Exception e) {
            if(e.getMessage().equals("ALREADY INSERTED")) {
                model.addAttribute("error_msg", "Page already indexed");
            }
            else {
                model.addAttribute("error_msg", "Error indexing page");
            }
            e.printStackTrace();
            model.addAttribute("validError", true);
            return "redirect:/addaddmany";
        }
        model.addAttribute("added", true);
        return "redirect:/";
    }

    @RequestMapping(value = "progress", method = RequestMethod.GET)
    public @ResponseBody String getProgress() {
        try {
            if (urlService == null || urlService.getMaxPages() == null || urlService.getProcessedPages() == null) {
                return "null";
            }
            int progress = (int) (urlService.getProcessedPages().intValue() * 100.0 / urlService.getMaxPages().intValue());
            return String.valueOf(progress) + "\n";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "null";
    }

}