package agh.bd.indexer.controller;

import agh.bd.indexer.model.Url;
import agh.bd.indexer.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/url")
public class UrlController {
    @Autowired private UrlService urlService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showUrl(@PathVariable long id, ModelMap model) {
        Url url = urlService.getDetailedUrlById(id);
        model.addAttribute("url", url);
        model.addAttribute("sentences", url.getSentences());
        return "url";
    }
}
